/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("Conversions")
package com.gitlab.thomasreader.kuantity.converter

/**
 * Creates a converter from an arbitrary amount of converters. If all converters are multiplicative then they will
 * be folded into a single [MultiplicativeConverter]
 *
 * @param converters the conversions used to create a single conversion
 * @return [IdentityConverter] if conversions contains zero elements;
 * the conversion if conversions contains one element;
 * [MultiplicativeConverter] if all conversions are [MultiplicativeConverter];
 * [CompositeConverter] otherwise
 */
public fun converterOf(vararg converters: UnitConverter): UnitConverter {
    return when (converters.size) {
        0 -> IdentityConverter
        1 -> converters[0]
        else -> {
            return when ((converters.any { it !is MultiplicativeConverter })) {
                // If any aren't multiplicative: return as composite
                true -> {
                    when (converters.size == 2) {
                        true -> PairConverter(converters[0], converters[1])
                        false -> com.gitlab.thomasreader.kuantity.converter.CompositeConverter(converters)
                    }
                }
                // All are multiplicative so fold
                false -> {
                    var factor: Double = 1.0
                    converters.forEach { multiConv ->
                        factor *= (multiConv as MultiplicativeConverter).factor
                    }
                    when (factor == 1.0) {
                        true -> IdentityConverter
                        false -> FactorConverter(factor)
                    }
                }
            }
        }
    }
}

/**
 * Creates a [MultiplicativeConverter] from an arbitrary amount of multiplicative converters.
 *
 * @param converters the conversions to be folded
 * @return the folded multiplicative conversion
 */
public fun converterOf(vararg converters: MultiplicativeConverter): MultiplicativeConverter {
    return when (converters.size) {
        0 -> IdentityConverter
        1 -> converters[0]
        else -> {
            var factor: Double = 1.0
            converters.forEach { multiUnit ->
                factor *= multiUnit.factor
            }
            when (factor == 1.0) {
                true -> IdentityConverter
                false -> FactorConverter(factor)
            }
        }
    }
}

/**
 * Appends that conversion to the this.
 *
 * @param that conversion to be appended
 * @return the conversion appended by that
 */
public fun UnitConverter.append(that: UnitConverter): UnitConverter {
    return when {
        this is IdentityConverter -> that
        that is IdentityConverter -> this
        this is MultiplicativeConverter && that is MultiplicativeConverter -> this * that
        else -> PairConverter(this, that)
    }
}

/**
 * Prepend that conversion to this.
 *
 * @param that conversion to be prepended
 * @return the conversion prepended by that
 */
public fun UnitConverter.prepend(that: UnitConverter): UnitConverter {
    return when {
        this is IdentityConverter -> that
        that is IdentityConverter -> this
        this is MultiplicativeConverter && that is MultiplicativeConverter -> that * this
        else -> PairConverter(that, this)
    }
}

/**
 * Fold two multiplicative conversions together.
 *
 * @param that conversion to be folded
 * @return the folded conversions
 */
public operator fun MultiplicativeConverter.times(that: MultiplicativeConverter): MultiplicativeConverter {
    val factor = this.factor * that.factor
    return when (factor == 1.0) {
        true -> IdentityConverter
        false -> FactorConverter(factor)
    }
}

/**
 * Divide this conversion by that conversion: useful for derived units.
 *
 * @param that the conversion to divide this by
 * @return this / that
 */
public operator fun MultiplicativeConverter.div(that: MultiplicativeConverter): MultiplicativeConverter {
    val factor = this.factor / that.factor
    return when (factor == 1.0) {
        true -> IdentityConverter
        false -> FactorConverter(factor)
    }
}