/*
 * Copyright (c) 2021 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.converter

import kotlin.math.pow

/**
 * Specialisation of a [MultiplicativeConverter] modelled by a base raised to the exponent.
 *
 * @property base the base
 * @property exponent the exponent
 * @constructor Create Power conversion of base^exponent
 */
public class PowerConverter(
    public val base: Int,
    public val exponent: Int
): MultiplicativeConverter {
    public override val factor: Double = base.toDouble().pow(exponent)

    public override fun toReference(value: Double): Double {
        return value * factor
    }
    public override fun fromReference(referenceValue: Double): Double {
        return referenceValue / this.factor
    }

    override fun pow(exponent: Int): MultiplicativeConverter {
        return when (exponent) {
            0 -> IdentityConverter
            1 -> this
            else -> PowerConverter(base, this.exponent * exponent)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PowerConverter

        if (base != other.base) return false
        if (exponent != other.exponent) return false

        return true
    }

    override fun hashCode(): Int {
        var result = base
        result = 31 * result + exponent
        return result
    }
}