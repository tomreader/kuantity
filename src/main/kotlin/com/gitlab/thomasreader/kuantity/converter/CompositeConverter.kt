/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.converter

/**
 * Models a conversion composed by an arbitrary amount of conversions.
 *
 * @property converters conversions this class composites
 * @constructor Create Composite conversion
 */
public class CompositeConverter(
    private val converters: Array<out com.gitlab.thomasreader.kuantity.converter.UnitConverter>
): com.gitlab.thomasreader.kuantity.converter.UnitConverter {
    public override fun toReference(value: Double): Double {
        var result = value
        this.converters.forEach { conversion ->
            result = conversion.toReference(result)
        }
        return result
    }

    public override fun fromReference(referenceValue: Double): Double {
        var result = referenceValue
        val start = this.converters.size - 1
        for (i in start downTo 0) {
            result = this.converters[i].fromReference(result)
        }
        return result
    }

    /**
     * Get the conversion at the specified index
     *
     * @param index
     * @return the conversion at the index
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     */
    public operator fun get(index: Int): com.gitlab.thomasreader.kuantity.converter.UnitConverter = this.converters[index]

    /**
     * Get the size of this composite conversion.
     */
    public val size: Int get() = this.converters.size

    /**
     * Creates an iterator for iterating over the conversions in this conversion.
     *
     * @return the iterator
     */
    public operator fun iterator(): Iterator<com.gitlab.thomasreader.kuantity.converter.UnitConverter> = this.converters.iterator()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as com.gitlab.thomasreader.kuantity.converter.CompositeConverter

        if (!converters.contentEquals(other.converters)) return false

        return true
    }

    override fun hashCode(): Int {
        return converters.contentHashCode()
    }
}