/*
 * Copyright (c) 2021 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.converter

/**
 * Unit conversion designed to convert to and from the reference unit.
 */
public interface UnitConverter {
    /**
     * Converts the current value to the value with the reference unit.
     *
     * For example converting from kilometres to the reference unit metre is a
     * conversion consisting of multiplication by 1000.
     *
     * @param value the value to be converted
     * @return the converted value in relation to the reference unit
     */
    public fun toReference(value: Double): Double

    /**
     * Converts the reference value to the value in relation to a unit which this conversion represents. This is the
     * inverse of [toReference]:
     *     x == fromReference(toReference(x))
     *
     * @param referenceValue
     * @return
     */
    public fun fromReference(referenceValue: Double): Double
}