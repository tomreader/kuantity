/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit.prefix

import com.gitlab.thomasreader.kuantity.converter.MultiplicativeConverter
import com.gitlab.thomasreader.kuantity.converter.PowerConverter

/**
 * Models a [metric prefix](https://en.wikipedia.org/wiki/Metric_prefix)that precedes a basic unit of measure
 * to indicate a multiple or fraction of the unit.
 *
 * @property symbol the symbol of the prefix
 * @param exponent the exponent raised to a base of ten this prefix symbolises
 */
public enum class MetricPrefix(
    public override val symbol: String,
    exponent: Int
) : UnitPrefix {
    /**
     * Yotta 10<sup>24</sup>
     */
    YOTTA("Y", 24),

    /**
     * Zetta 10<sup>21</sup>
     */
    ZETTA("Z", 21),

    /**
     * Exa 10<sup>18</sup>
     */
    EXA("E", 18),

    /**
     * Peta 10<sup>15</sup>
     */
    PETA("P", 15),

    /**
     * Tera 10<sup>12</sup>
     */
    TERA("T", 12),

    /**
     * Giga 10<sup>9</sup>
     */
    GIGA("G", 9),

    /**
     * Mega 10<sup>6</sup>
     */
    MEGA("M", 6),

    /**
     * Kilo 10<sup>3</sup>
     */
    KILO("k", 3),

    /**
     * Hecto 10<sup>2</sup>
     */
    HECTO("h", 2),

    /**
     * Deka 10<sup>1</sup>
     */
    DEKA("da", 1),

    /**
     * Deci 10<sup>-1</sup>
     */
    DECI("d", -1),

    /**
     * Centi 10<sup>-2</sup>
     */
    CENTI("c", -2),

    /**
     * Milli 10<sup>-3</sup>
     */
    MILLI("m", -3),

    /**
     * Micro 10<sup>-6</sup>
     */
    MICRO("\u00B5", -6), // μ \u00C2 \u00B5

    /**
     * Nano 10<sup>-9</sup>
     */
    NANO("n", -9),

    /**
     * Pico 10<sup>-12</sup>
     */
    PICO("p", -12),

    /**
     * Femto 10<sup>-15</sup>
     */
    FEMTO("f", -15),

    /**
     * Atto 10<sup>-18</sup>
     */
    ATTO("a", -18),

    /**
     * Zepto 10<sup>-21</sup>
     */
    ZEPTO("z", -21),

    /**
     * Yocto 10<sup>-24</sup>
     */
    YOCTO("y", -24);

    public override val converter: MultiplicativeConverter

    init {
        converter = PowerConverter(10, exponent)
    }
}