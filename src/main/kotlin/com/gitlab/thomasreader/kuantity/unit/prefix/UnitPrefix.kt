/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit.prefix

import com.gitlab.thomasreader.kuantity.converter.MultiplicativeConverter
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure

/**
 * This interface models a [unit prefix](https://en.wikipedia.org/wiki/Unit_prefix).
 */
public interface UnitPrefix {
    public val symbol: String
    public val converter: MultiplicativeConverter
}

/**
 * Prefix the symbol of the provided unit.
 *
 * @param unit unit whose symbol is to be prefixed
 * @return the prefixed symbol or null if the unit symbol is null
 */
public fun UnitPrefix.prefixSymbol(unit: UnitOfMeasure<*>): String? = when (unit.symbol.isNullOrBlank()) {
    true -> null
    false -> this.symbol + unit.symbol
}