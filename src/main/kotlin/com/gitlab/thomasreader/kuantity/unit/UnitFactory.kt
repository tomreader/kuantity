/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("UnitFactory")
package com.gitlab.thomasreader.kuantity.unit

import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.converter.FactorConverter
import com.gitlab.thomasreader.kuantity.converter.IdentityConverter
import com.gitlab.thomasreader.kuantity.quantity.QuantityType


/**
 * Helper factory method to create a [UnitOfMeasure] from a symbol and factor.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param symbol the symbol or abbreviation of this unit — nullable
 * @param factor the multiplicative factor to convert this unit to the reference unit
 * @return a new unit of measurement
 */
public fun <T: QuantityType> unitOfMeasurement(
    symbol: String?,
    factor: Double
): UnitOfMeasure<T> {
    return when (factor == 1.0) {
        true -> UnitOfMeasureImpl<T>(symbol, IdentityConverter)
        false -> UnitOfMeasureImpl<T>(symbol, FactorConverter(factor))
    }
}

/**
 * Helper factory method to create a [UnitOfMeasure] from a symbol and [UnitConverter].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param symbol the symbol or abbreviation of this unit — nullable
 * @param converter the conversion to convert this unit to the reference unit
 * @return a new unit of measurement
 */
public fun <T: QuantityType> unitOfMeasurement(
    symbol: String?,
    converter: UnitConverter
): UnitOfMeasure<T> = UnitOfMeasureImpl<T>(symbol, converter)