/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity.range

import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.QuantityType

/**
 * A closed range of Quantities with the same dimension.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @constructor
 * @param start the minimum quantity in the range
 * @param endInclusive the maximum quantity in the range
 */
public class QuantityRange<T: QuantityType>(
    start: Quantity<T>,
    endInclusive: Quantity<T>
): ClosedRange<Quantity<T>> {
    private val _start = start
    private val _endInclusive = endInclusive

    public override val endInclusive: Quantity<T> get() = this._endInclusive
    public override val start: Quantity<T> get() = this._start

    public override fun contains(value: Quantity<T>): Boolean = value >= this._start && value <= this._endInclusive
    public override fun isEmpty(): Boolean = this._start > this._endInclusive

    public override fun equals(other: Any?): Boolean {
        return other is QuantityRange<*> && (this.isEmpty() && other.isEmpty() ||
                this._start == other._start && this._endInclusive == other._endInclusive)
    }

    public override fun hashCode(): Int {
        return if (this.isEmpty()) -1 else 31 * this._start.value.hashCode() + this._endInclusive.value.hashCode()
    }

    public override fun toString(): String = "${this._start.value}..${this._endInclusive.value}"
}