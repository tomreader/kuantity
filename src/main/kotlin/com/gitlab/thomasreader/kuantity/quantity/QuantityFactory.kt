/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity

import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure

/**
 * Helper method to easily create a [Quantity] from a [Double] and [UnitOfMeasure].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param unit the unit that this value is represented by
 * @return a new Quantity
 */
public operator fun <T: QuantityType> Double.times(unit: UnitOfMeasure<T>): Quantity<T> =
    Quantity(unit.converter.toReference(this))

/**
 * Helper method to easily create a [Quantity] from a [Double] and [UnitOfMeasure].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param value the value that this unit is multiplied by
 * @return a new Quantity
 */
public operator fun <T: QuantityType> UnitOfMeasure<T>.times(value: Double): Quantity<T> = value * this

/**
 * Inline helper method to easily create a [Quantity] from a [Number] and [UnitOfMeasure].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param unit the unit that this value is represented by
 * @return a new Quantity
 */
public inline operator fun <T: QuantityType> Number.invoke(unit: UnitOfMeasure<T>): Quantity<T> = this.toDouble() * unit

/**
 * Inline helper method to easily create a [Quantity] from a [Number] and [UnitOfMeasure].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param value the value that this unit is multiplied by
 * @return a new Quantity
 */
public inline operator fun <T: QuantityType> UnitOfMeasure<T>.invoke(value: Number): Quantity<T> = value.toDouble() * this

/**
 * Inline helper method to easily create a [Quantity] from a [Number] and [UnitOfMeasure].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param unit the unit that this value is represented by
 * @return a new Quantity
 */
public inline operator fun <T: QuantityType> Number.times(unit: UnitOfMeasure<T>): Quantity<T> = this.toDouble() * unit

/**
 * Inline helper method to easily create a [Quantity] from a [Number] and [UnitOfMeasure].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param value the value that this unit is multiplied by
 * @return a new Quantity
 */
public inline operator fun <T: QuantityType> UnitOfMeasure<T>.times(value: Number): Quantity<T> = value.toDouble() * this