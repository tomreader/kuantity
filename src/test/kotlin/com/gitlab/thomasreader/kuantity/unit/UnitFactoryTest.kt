/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit

import com.gitlab.thomasreader.kuantity.converter.FactorConverter
import com.gitlab.thomasreader.kuantity.converter.IdentityConverter
import com.gitlab.thomasreader.kuantity.converter.MultiplicativeConverter
import com.gitlab.thomasreader.kuantity.quantity.Dimensionless
import com.gitlab.thomasreader.kuantity.quantity.Length
import org.junit.Test

import org.junit.Assert.*

class UnitFactoryTest {

    @Test
    fun unitOfMeasurement() {
        val metre = unitOfMeasurement<Length>("m", IdentityConverter)
        val inch = unitOfMeasurement<Length>("in", FactorConverter(0.0254))
        val one = unitOfMeasurement<Dimensionless>(null, IdentityConverter)

        assertEquals("m", metre.symbol)
        assertEquals(IdentityConverter, metre.converter)

        assertEquals("in", inch.symbol)
        assertEquals(FactorConverter(0.0254), (inch.converter as MultiplicativeConverter))

        assertEquals(null, one.symbol)
        assertEquals(IdentityConverter, one.converter)
    }

    @Test
    fun unitOfMeasurementFactor() {
        val metre = unitOfMeasurement<Length>("m", 1.0)
        val inch = unitOfMeasurement<Length>("in", 0.0254)
        val one = unitOfMeasurement<Dimensionless>(null, 1.0)

        assertEquals("m", metre.symbol)
        assertEquals(IdentityConverter, metre.converter)

        assertEquals("in", inch.symbol)
        assertEquals(FactorConverter(0.0254), (inch.converter as MultiplicativeConverter))

        assertEquals(null, one.symbol)
        assertEquals(IdentityConverter, one.converter)
    }
}