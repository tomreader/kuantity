/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit.prefix

import com.gitlab.thomasreader.kuantity.converter.IdentityConverter
import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.quantity.Dimensionless
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.Temperature
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure
import com.gitlab.thomasreader.kuantity.unit.prefix.MetricPrefix.*
import com.gitlab.thomasreader.kuantity.unit.prefix.BinaryPrefix.*
import org.junit.Test

import org.junit.Assert.*

class UnitPrefixKtTest {

    @Test
    fun prefixSymbol() {
        val metre = object: UnitOfMeasure<Length> {
            override val symbol: String? = "m"
            override val converter: UnitConverter = IdentityConverter
        }

        val one = object: UnitOfMeasure<Dimensionless> {
            override val symbol: String? = null
            override val converter: UnitConverter = IdentityConverter
        }

        val kelvin = object: UnitOfMeasure<Temperature> {
            override val symbol: String? = "K"
            override val converter: UnitConverter = IdentityConverter
        }

        val bit = object: UnitOfMeasure<Nothing> {
            override val symbol: String? = "b"
            override val converter: UnitConverter = IdentityConverter
        }

        assertEquals("km", KILO.prefixSymbol(metre))
        assertEquals(null, CENTI.prefixSymbol(one))
        assertEquals("cK", CENTI.prefixSymbol(kelvin))
        assertEquals("Mib", MEBI.prefixSymbol(bit))
    }
}