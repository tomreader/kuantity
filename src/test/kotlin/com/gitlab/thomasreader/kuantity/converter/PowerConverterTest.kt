package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class PowerConverterTest {

    val centiConversion = PowerConverter(10, -2)
    val kibiConversion = PowerConverter(2, 10)

    @Test
    fun convertToReference() {
        assertEquals(1.0, centiConversion.toReference(100.0), 1.0E-7)
        assertEquals(1024.0, kibiConversion.toReference(1.0), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(100.0, centiConversion.fromReference(1.0), 1.0E-7)
        assertEquals(1.0, kibiConversion.fromReference(1024.0), 1.0E-7)
    }
}