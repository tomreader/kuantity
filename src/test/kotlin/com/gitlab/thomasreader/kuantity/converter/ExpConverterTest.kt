package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class ExpConverterTest {

    val belConversion = ExpConverter(10.0)
    val neperConversion = ExpConverter(Math.E)

    @Test
    fun convertToReference() {
        assertEquals(1.2589254, belConversion.toReference(0.1), 1.0E-7)
        assertEquals(10000.0, belConversion.toReference(4.0), 1.0E-7)
        assertEquals(Math.E, neperConversion.toReference(1.0), 1.0E-7)
        assertEquals(20.0855369231, neperConversion.toReference(3.0), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(9.0, belConversion.fromReference(1_000_000_000.0), 1.0E-7)
        assertEquals(1.0, neperConversion.fromReference(Math.E), 1.0E-7)
    }
}