package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class LogConverterTest {

    val inverseBelConversion = LogConverter(10.0)
    val inverseNeperConversion = LogConverter(Math.E)

    @Test
    fun convertToReference() {
        assertEquals(9.0, inverseBelConversion.toReference(1_000_000_000.0), 1.0E-7)
        assertEquals(1.0, inverseNeperConversion.toReference(Math.E), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(1.2589254, inverseBelConversion.fromReference(0.1), 1.0E-7)
        assertEquals(10000.0, inverseBelConversion.fromReference(4.0), 1.0E-7)
        assertEquals(Math.E, inverseNeperConversion.fromReference(1.0), 1.0E-7)
        assertEquals(20.0855369231, inverseNeperConversion.fromReference(3.0), 1.0E-7)
    }
}