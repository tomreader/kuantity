package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class ConvertersKtTest {

    @Test
    fun conversionsOf() {
        val factorConversion = FactorConverter(50.0)
        assertEquals(factorConversion, converterOf(factorConversion))
        val offsetConversion = OffsetConverter(10.0)
        val pairConversionExpected = PairConverter(factorConversion, offsetConversion)
        val pairConversion = converterOf(factorConversion, offsetConversion)
        assertEquals(pairConversionExpected.converter1, (pairConversion as PairConverter).converter1)
        assertEquals(pairConversionExpected.converter2, (pairConversion as PairConverter).converter2)
    }

    @Test
    fun multiConversionsOf() {
        assertEquals(50.0, converterOf(FactorConverter(50.0)).factor, 0.0)
        assertEquals(2500.0, converterOf(FactorConverter(50.0), FactorConverter(50.0)).factor, 0.0)
    }

    @Test
    fun append() {
        val factorConversion = FactorConverter(50.0)
        val offsetConversion = OffsetConverter(10.0)

        assertEquals(factorConversion, factorConversion.append(IdentityConverter))
        assertEquals(2500.0, (factorConversion.append(factorConversion) as MultiplicativeConverter).factor, 0.0)
        assertEquals(PairConverter::class.java, factorConversion.append(offsetConversion)::class.java)
    }

    @Test
    fun prepend() {
        val factorConversion = FactorConverter(50.0)
        val offsetConversion = OffsetConverter(10.0)

        assertEquals(factorConversion, factorConversion.prepend(IdentityConverter))
        assertEquals(2500.0, (factorConversion.prepend(factorConversion) as MultiplicativeConverter).factor, 0.0)
        assertEquals(PairConverter::class.java, factorConversion.prepend(offsetConversion)::class.java)
    }

    @Test
    fun times() {
        assertEquals(2500.0, FactorConverter(50.0).times(FactorConverter(50.0)).factor, 0.0)
    }

    @Test
    fun div() {
        assertEquals(1.0, FactorConverter(50.0).div(FactorConverter(50.0)).factor, 0.0)
    }
}