package com.gitlab.thomasreader.kuantity.quantity

import org.junit.Test

import org.junit.Assert.*

class MapKtTest {

    @Test
    fun mapValue() {
        assertEquals(100.0, Quantity<Nothing>(50.0).mapValue { it * 2 }.value, 1.0E-7)
    }

    @Test
    fun mapValueIf() {
        assertEquals(
            100.0,
            Quantity<Nothing>(50.0).mapValueIf(
                predicate = { it == 50.0 }, mapper = { 100.0 }
            ).value,
            1.0E-7
        )

        assertEquals(
            0.0,
            Quantity<Nothing>(0.0000000000123).mapValueIf(
                predicate = { it < 0.00001 }, mapper = { 0.0 }
            ).value,
            1.0E-7
        )
    }

    @Test
    fun map() {
        assertEquals(100.0, Quantity<Nothing>(50.0).map { it * 2.0 }.value, 1.0E-7)
    }

    @Test
    fun mapIf() {
        assertEquals(
            0.0,
            Quantity<Nothing>(0.0000000000123).mapIf(
                { it.isZero(0.00001) }, { Quantity(0.0) }
            ).value,
            0.0
        )
    }
}